﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Util;
using Android.Content;
using Android.Runtime;
using Android.Widget;
using Android.Views;
using Android.Util;

using RU.Nilsoft.TM;
using System.Threading;

[assembly: UsesPermission("com.pos.permission.ACCESSORY_DATETIME")]
[assembly: UsesPermission("com.pos.permission.ACCESSORY_LED")]
[assembly: UsesPermission("com.pos.permission.ACCESSORY_BEEP")]
[assembly: UsesPermission("com.pos.permission.ACCESSORY_RFREGISTER")]
[assembly: UsesPermission("com.pos.permission.CARD_READER_ICC")]
[assembly: UsesPermission("com.pos.permission.CARD_READER_PICC")]
[assembly: UsesPermission("com.pos.permission.CARD_READER_MAG")]
[assembly: UsesPermission("com.pos.permission.COMMUNICATION")]
[assembly: UsesPermission("com.pos.permission.PRINTER")]
[assembly: UsesPermission("com.pos.permission.SECURITY")]
[assembly: UsesPermission("com.pos.permission.ACCESSORY_RFREGISTER")]
[assembly: UsesPermission("com.pos.permission.EMVCORE")]

[assembly: UsesPermission("android.permission.WRITE_EXTERNAL_STORAGE")]
[assembly: UsesPermission("android.permission.READ_EXTERNAL_STORAGE")]
[assembly: UsesPermission("android.permission.ACCESS_WIFI_STATE")]
[assembly: UsesPermission("android.permission.ACCESS_NETWORK_STATE")]
[assembly: UsesPermission("android.permission.INTERNET")]
[assembly: UsesPermission("android.permission.RECEIVE_BOOT_COMPLETED")]

namespace AppTac
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private static TMLib libTM;
        private static TMAzimuth tmAZ;

        ProgressDialog progress;

        private void showProgressDialog(string text)
        {
            RunOnUiThread(() => {
                progress.Indeterminate = true;
                progress.SetProgressStyle(ProgressDialogStyle.Spinner);

                progress.SetMessage(text);
                progress.SetCancelable(false);
                progress.Show();
            });
        }

        private void hideProgressDialog()
        {
            RunOnUiThread(() => {
                progress.Hide();
            });
            
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            progress = new ProgressDialog(this);
        }

        protected override void OnStart()
        {
            initAzimuthLib();
            base.OnStart();
            
            progress.Hide();
        }
        // иициализация библиотеки для работы с ККТ
        private void initAzimuthLib()
        {

            //initAzimuthLib();

            //SystemPrint.Init();

            if (libTM == null) { 
                libTM = TMLib.Instance;

                //libTM.IsLogSet(true);
                libTM.TMLibLoadFR();
                libTM.TMLibLoadPrint(Application.Context);
                libTM.SetGrayInt(1000);

                tmAZ = new TMAzimuth(libTM);
            }

        }
        // активация ККТ
        private bool initKKT()
        {
            bool ret = false;

            //byte[] dataDLE0 = { 0x10, 0x30 };
            //byte[] dataDLE9 = { 0x10, 0x39 };

            //var iDLE0 = libTM.DoCmd(dataDLE0, 1000);
            //var iDLE9 = libTM.DoCmd(dataDLE9, 1000);

            //iDLE0 = libTM.DoCmd(dataDLE0, 1000);
            //iDLE9 = libTM.DoCmd(dataDLE9, 1000);

            //if (((byte)iDLE0 & TMLib.States.Init) == 0)
            //{
            //    ret = false;
            //}
            //else
            //{
            //    ret = true;
            //}

            //!** Статус контроллера
            int res = 0;
            string inf = "";

            int ofd = tmAZ.EnvelopesCount;


            if (libTM.ActivateNew() != 0)
            {
                ret = false;
            }
            else
            {
                ret = true;
            }


            return ret;
        }
        // печать не фискального документа
        private void PrintOEMDoc()
        {
            var txt = "1234567890 русский russian";
            tmAZ.OpenFDoc();
            tmAZ.PrintOEMDoc(txt, txt.Length);
            tmAZ.CloseFDoc();
        }
        
        // печать произвольного фискального документа
        private  void PrintFDoc()
        {

            showProgressDialog("Произвольный фискальный документ.");

            new Thread(new ThreadStart(() => {

                int er = tmAZ.OpenFiscalDocEx(0, 0, 0, 1, 1, 1, 40, 1, 24, "101");
                er = tmAZ.PdfPaint(6, 6, 500, "TestPDF417");

                tmAZ.AddSerPosField(1, 1, 0);
                tmAZ.AddDocPosField(2, 1, 0);
                tmAZ.AddDatePosField(3, 1, 0);
                tmAZ.AddTimePosField(4, 1, 0);
                tmAZ.AddINNPosField(15, 1, 0);
                tmAZ.AddOperPosField(14, 1, 0);
                tmAZ.AddSumPosField(15, 1, 0);

                for (int i = 0; i < 10; i++)
                {
                    er = tmAZ.AddFreeField((1), (i + 3), 0, 1, 0, "Номер строки:" + (i + 1).ToString());
                }
                er = tmAZ.AddFreeField(1, 16, 0, 1, 0, "<1030>5555|10.1|1|01010101|");
                er = tmAZ.AddFreeField(1, 17, 0, 1, 0, "<1038>");
                er = tmAZ.AddFreeField(1, 18, 0, 1, 0, "<1042>");
                er = tmAZ.AddFreeField(1, 19, 0, 1, 0, "<1055>01");
                //        er = tmAZ.addFreeField(1,14,0,1,0,"<1009>123");
                //        er = tmAZ.addFreeField(1,14,0,1,0,"<1187>123");
                //        er = tmAZ.AddFreeField(0, 0, 0, 2, 0,"");
                //        er = tmAZ.AddFreeField(1, 1, 0, 1, 30, "J01H00f00h02w02k4aT0");
                er = tmAZ.AddFreeField(1, 0, 1, 1, 30, "J16h30H00w02f31J01R00k49T7B43000B6207564110380B");
                er = tmAZ.AddFreeField(1, 0, 1, 1, 30, "J16h30H00w02f31J01R00k49T7B43000B6207564110380B");

                er = tmAZ.PrintFiscalReceipt();

                RunOnUiThread(() => {
                    Toast.MakeText(this, "PrintFiscalReceipt: " + er.ToString(), ToastLength.Short).Show();
                });
                

                hideProgressDialog();

            })).Start();
        }


        private void PrintChequeDoc()
        {
            tmAZ.StartSeans();

            int er = tmAZ.StartReceipt(0, "Ivanov|780000000001", 1, 0, "", "", "");
            er = tmAZ.CommentReceipt("<1009>123|<1187>123|");
            er = tmAZ.ItemReceipt("Молоко", "", "шт.", "", "", 000, 1000, 0x0101, 01, 01, 00);
            er = tmAZ.TotalReceipt("");
            er = tmAZ.CommentReceipt("Линия примера");
            er = tmAZ.TenderReceipt(0, 000, "", "");
            er = tmAZ.CloseReceipt();

            Toast.MakeText(this, "closeReceipt: " + er.ToString(), ToastLength.Short).Show();
        }



        // отправка в ОФД
        private void SendOFD()
        {
            TMCommand cmd = new TMCommand();
            cmd.CmdGetWaitDocs();
            libTM.DoCmd(cmd, 1000);

            TMCommand cmd_r;
            cmd_r = libTM.LastResCMD;

            int waitDocs = '\uffff' & cmd_r.GetFieldHexW(5);

            string ofd_text = "";
            ofd_text += "Сообщений для отправки: " + waitDocs;

            Toast.MakeText(this, ofd_text, ToastLength.Short).Show();

            showProgressDialog("Идет отправка в ОФД.");

            new Thread(new ThreadStart(() => {
                byte[] ofd_send = tmAZ.FsReadBlockData();
                byte[] ofd_read = new byte[] { };

                if (ofd_send != null)
                {
                    if (ofd_send.Length > 0)
                    {
                        ofd_read = tmAZ.SendData(ofd_send, "" , 0);
                    }
                }

                if (ofd_read != null)
                {
                    if (ofd_read.Length > 0)
                    {
                        byte[] shortAnswer = new byte[ofd_read.Length - 30];
                        System.Buffer.BlockCopy(ofd_read, 30, shortAnswer, 0, ofd_read.Length - 30);
                        tmAZ.FsWriteBlockData(shortAnswer);
                    }
                }

                hideProgressDialog();

            })).Start();

        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        // инициализация состояния
        [Java.Interop.Export("cmd_init_OnClick")]
        public void cmd_init_OnClick(View v)
        {
            bool res = initKKT();
            string txt = "";
            if (res == false)
            {
                txt = "ККТ не активированна";
            }else
            {
                txt = "ККТ активированна";
            }
            
            Toast.MakeText(this, "initKKT: " + txt, ToastLength.Short).Show();
        }
        // отчет открытия смены
        [Java.Interop.Export("cmd_openShift_OnClick")]
        public void cmd_openShift_OnClick(View v)
        {
            int res = tmAZ.ShiftOpen("Иванов И.И.");
            Toast.MakeText(this, "ShiftOpen: " + res.ToString(), ToastLength.Short).Show();
        }
        // отчет закрытия смены
        [Java.Interop.Export("cmd_closeShift_OnClick")]
        public void cmd_closeShift_OnClick(View v)
        {
            int res = tmAZ.ShiftClose("Иванов И.И.");
            Toast.MakeText(this, "ShiftClose: " + res.ToString(), ToastLength.Short).Show();
        }
        // произвольный фискальный документ
        [Java.Interop.Export("cmd_FreeDoc_OnClick")]
        public void cmd_FreeDoc_OnClick(View v)
        {
            PrintFDoc();
        }
        // фискальный чек
        [Java.Interop.Export("cmd_chequeDoc_OnClick")]
        public void cmd_chequeDoc_OnClick(View v)
        {
            PrintChequeDoc();
        }
        // произвольный текстовый чек
        [Java.Interop.Export("cmd_printText_OnClick")]
        public void cmd_printText_OnClick(View v)
        {
            PrintOEMDoc();
            Toast.MakeText(this, "PrintOEMDoc ", ToastLength.Short).Show();
        }
        // отправка данных в ОФД
        [Java.Interop.Export("cmd_sendOFD_OnClick")]
        public void cmd_sendOFD_OnClick(View v)
        {
            SendOFD();
        }
        // состояние ККТ
        [Java.Interop.Export("cmd_statusKKT_OnClick")]
        public void cmd_statusKKT_OnClick(View v)
        {
            TMCommand cmd = new TMCommand();
            cmd.CmdGetInfo();
            libTM.DoCmd(cmd, 2000);

            string info = ""; //tmAZ.getFldStr(1 ) + "\n";
            info += tmAZ.GetFldStr(1) + "\n";
            info += tmAZ.GetFldStr(2) + "\n";
            info += tmAZ.GetFldStr(3) + "\n";
            info += tmAZ.GetFldStr(4) + "\n";
            info += tmAZ.GetFldStr(5) + "\n";
            info += tmAZ.GetFldStr(6) + "\n";
            info += tmAZ.GetFldStr(7) + "\n";
            info += tmAZ.GetFldStr(8) + "\n";
            info += tmAZ.GetFldStr(9) + "\n";
            info += tmAZ.GetFldStr(10) + "\n";
            info += tmAZ.GetFldStr(11) + "\n";

            Toast.MakeText(this, "info: " + info, ToastLength.Short).Show();

            tmAZ.FnGetCommStatus();
        }
    }


    [BroadcastReceiver]
    [IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted }, Categories = new[] { Android.Content.Intent.CategoryDefault})]
    public class ReceiveBoot : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            //Toast.MakeText(context, "Received intent!", ToastLength.Short).Show();
            Log.Debug("XamarinApp", "onReceive " + intent.Action);

            if ((intent.Action != null) && (intent.Action == Android.Content.Intent.ActionBootCompleted))
            { // Start the service or activity
              //context.ApplicationContext.StartService(new Intent(context, typeof(MainActivity)));

                Android.Content.Intent start = new Android.Content.Intent(context, typeof(MainActivity));

                // my activity name is MainActivity replace it with yours
                start.AddFlags(ActivityFlags.NewTask);
                context.ApplicationContext.StartActivity(start);
            }

            //if ((intent.Action != null) && (intent.Action == Android.Content.Intent.ActionUserPresent))
            //{ // Start the service or activity
            //  //context.ApplicationContext.StartService(new Intent(context, typeof(MainActivity)));

            //    Android.Content.Intent start = new Android.Content.Intent(context, typeof(MainActivity));

            //    // my activity name is MainActivity replace it with yours
            //    start.AddFlags(ActivityFlags.NewTask);
            //    context.ApplicationContext.StartActivity(start);
            //}
        }

        [BroadcastReceiver]
        [IntentFilter(new[] { Android.Content.Intent.ActionUserPresent, Android.Content.Intent.ActionScreenOn }, Priority = 100)]
        class ScreenOn : BroadcastReceiver
        {
            public override void OnReceive(Context context, Intent intent)
            {

                if ((intent.Action != null) && (intent.Action == Android.Content.Intent.ActionUserPresent))
                { 
                    // Start the service or activity
                    //context.ApplicationContext.StartService(new Intent(context, typeof(MainActivity)));

                    Android.Content.Intent start = new Android.Content.Intent(context, typeof(MainActivity));

                    // my activity name is MainActivity replace it with yours
                    start.AddFlags(ActivityFlags.NewTask);
                    context.ApplicationContext.StartActivity(start);
                }
            }
        }
    }


    //[BroadcastReceiver]
    //[IntentFilter(new[] { Android.Content.Intent.ActionUserPresent, Android.Content.Intent.ActionScreenOn }, Priority = 100)]
    //class ReceiveBoot : BroadcastReceiver
    //{
    //    public override void OnReceive(Context context, Intent intent)
    //    {
    //        Log.Info("Xamarin", "OnReceive");
    //        if ((intent.Action != null) && (intent.Action == Android.Content.Intent.ActionUserPresent))
    //        { // Start the service or activity
    //          //context.ApplicationContext.StartService(new Intent(context, typeof(MainActivity)));

    //            Android.Content.Intent start = new Android.Content.Intent(context, typeof(MainActivity));

    //            // my activity name is MainActivity replace it with yours
    //            start.AddFlags(ActivityFlags.NewTask);
    //            context.ApplicationContext.StartActivity(start);
    //        }
    //    }
    //}
}